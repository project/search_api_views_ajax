Search API Views Ajax
================
Search API Views Ajax facilitates updating a search view asynchronously when 
using an AJAX enabled view with the Search API module. In particular it:

- Enables updating the query string on a view when using an exposed 
`full text search field`. e.g. `/projects?search=drupal`
- Adds a popstate event listener to the DOM to facilitate updating the async 
enabled view asynchronously when navigating when using the web browser's back 
button.
- Support for using with an exposed form in a block.

This module is useful as it allows users to bookmark and share urls that are 
generated using a search api search index and view. 

Additionally, Search API Views Ajax also updates the browser history 
asynchronously, allowing you to manually navigate back and forth in the browser.
Loading the content currently requires a manual reload in the browser.

Other use cases
------------
This modules primary use case is when using the contributed search_api with a 
solr generated index and view. However, this module should also work in other 
instances where an exposed `full text search field` is being used with an ajax 
enabled view.

Alternatives
------------
<a href="https://www.drupal.org/project/views_ajax_history" 
title="Views Ajax History">Views Ajax History</a> also updates the query string
and history when using an ajax enabled view. However this also includes the page
number which can lead to undesirable behaviour when navigating back to the page
from another tab.

Installation
------------
Download and install like any other Drupal module.

Using
------------
Once installed you may need to clear your cache.

Todo
----
- Review search view asynchronously when navigating back and forth so that user
does not need to reload manually. Currently this only reloads when you click 
once on back button.
- Make use of config api.

Maintainer
-----------
- Daniel Lobo (2dareis2do)
- If you are interested in becoming a co-maintainer, please get in touch!
